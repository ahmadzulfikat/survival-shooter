﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
    public PlayerHealth playerHealth;
    //public int heal = 20;

    private void OnCollisionEnter(Collision playerHeal)
    {
        if (playerHeal.gameObject.tag == "Player")
        {
            Healing(10);
            
            Destroy(gameObject);
        }
    }

    public void Healing(int heal)
    {
        if (playerHealth.healthSlider.value >= 100)
        {
            playerHealth.currentHealth = 100;
        }
        else
        {
            playerHealth.currentHealth += heal;

            playerHealth.healthSlider.value = playerHealth.currentHealth;
        }
    }
}
