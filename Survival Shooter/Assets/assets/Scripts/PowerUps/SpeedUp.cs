﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour
{
    public PlayerMovement playerMovement;

    MeshRenderer speedItemRenderer;
    SphereCollider itemCollider;
    public GameObject halo;

    private void Start()
    {
        speedItemRenderer = GetComponent<MeshRenderer>();
        itemCollider = GetComponent<SphereCollider>();
    }

    private void OnCollisionEnter(Collision playerSpeed)
    {
        if(playerSpeed.gameObject.tag == "Player")
        {
            playerMovement.speed = 10;
            speedItemRenderer.enabled = false;
            itemCollider.enabled = false;
            Destroy(halo);

            StartCoroutine(PowerUpTimer(10));
        }
    }

    IEnumerator PowerUpTimer(int time)
    {
        yield return new WaitForSeconds(time);
        playerMovement.speed = 6;
        Destroy(gameObject);
    }
}
